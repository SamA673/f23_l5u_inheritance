package polymorphism;

public class BookStore {
    

    public static void main(String []args){
        Book[] shelf = new Book[]{new Book("Book A", "Alpha"), 
                                  new ElectronicBook("EBook B", "Bravo", 23),
                                  new Book("Book C", "Charlie"), 
                                  new ElectronicBook("EBook D", "Delta", 50),
                                  new ElectronicBook("EBook E", "Echo",25)};

        for (Book book : shelf){
                System.out.println(book); 
        }
 


        ElectronicBook b = (ElectronicBook)shelf[1];
        //ElectronicBook c = (ElectronicBook)shelf[0]; 

        System.out.println(b.getNumberBytes()); 
        //System.out.println(c.getNumberBytes()); 


    }
}
