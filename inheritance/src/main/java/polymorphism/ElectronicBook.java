package polymorphism; 

public class ElectronicBook extends Book {
    private int numberBytes; 


    public ElectronicBook(String title, String author, int numBytes){
        super(title, author);
        this.numberBytes = numBytes; 
    }


    // Getter
    public int getNumberBytes(){
        return this.numberBytes; 
    }
    

    // toString
    public String toString(){
        return super.toString() + " (" + this.numberBytes + "B)"; 
    }


}
