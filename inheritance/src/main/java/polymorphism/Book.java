package polymorphism;

public class Book {
    protected String title;
    private String author;


    // Constructor 
    public Book (String title, String author){
        this.title = title; 
        this.author = author; 
    }


    // Getters
    public String getTitle(){
        return this.title; 
    }
    
    public String author(){
        return this.author; 
    }

    // toString
    public String toString(){
        return this.title + ", by " + this.author; 
    }


    
}
